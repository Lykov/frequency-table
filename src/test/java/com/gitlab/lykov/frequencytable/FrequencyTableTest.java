package com.gitlab.lykov.frequencytable;

import com.gitlab.lykov.frequencytable.worker.Worker;
import com.gitlab.lykov.frequencytable.worker.WorkerResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class FrequencyTableTest {
    private static WorkerResult expected;

    @BeforeAll
    public static void setUp() {
        Map<Character, Integer> map = new TreeMap<>(Collections.reverseOrder());
        map.put('a', 2);
        map.put('b', 2);
        map.put('c', 2);
        expected = new WorkerResult(map, 2);
    }

    @Test
    @DisplayName("should process 'AbC, abc?' correctly")
    public void workerTest() {
        Worker worker = new Worker();
        WorkerResult result = worker.process(Stream.of("AbC,", "abc?"));
        Assertions.assertEquals(expected, result);
    }

    @Test
    @DisplayName("scanner should catch up created file")
    public void scannerTest() throws Exception {
        Path in = Paths.get("TEST_IN/test.txt");
        Path dirIn = Paths.get("TEST_IN");
        Path dirOut = Paths.get("TEST_OUT");
        Files.createDirectory(dirIn);
        Files.createDirectory(dirOut);
        Files.write(in, "abc bca???".getBytes(StandardCharsets.UTF_8));
        com.gitlab.lykov.frequencytable.Scanner scanner = new Scanner(Executors.newScheduledThreadPool(5), 10, TimeUnit.SECONDS,
                Paths.get("TEST_IN"), Paths.get("TEST_OUT"));
        scanner.start();
        Files.write(in, "abc bca???".getBytes(StandardCharsets.UTF_8));
        Thread.sleep(10000);
        scanner.stop();
        Path out = Paths.get("TEST_OUT/test.txt");
        String content = new String(Files.readAllBytes(out));
        Assertions.assertEquals(expected.toString(), content);

        Files.delete(in);
        Files.delete(out);
        Files.delete(dirIn);
        Files.delete(dirOut);
    }
}
