package com.gitlab.lykov.frequencytable;

import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(Executors.newScheduledThreadPool(5), 10, TimeUnit.SECONDS,
                Paths.get("IN"), Paths.get("OUT"));
        scanner.start();
    }
}
