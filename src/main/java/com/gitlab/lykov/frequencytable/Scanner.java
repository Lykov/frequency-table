package com.gitlab.lykov.frequencytable;

import com.gitlab.lykov.frequencytable.worker.Worker;
import com.gitlab.lykov.frequencytable.worker.WorkerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * Периодически сканирует входную директорию на наличие необработанных файлов, запускает для каждого файла Worker
 * и выводит WorkerResult в файл
 */
public final class Scanner {
    private static final Logger log = LoggerFactory.getLogger(Scanner.class);

    private final ScheduledExecutorService scheduler;
    private final long scanningPeriod;
    private final TimeUnit scanningTimeUnit;
    private final Path in;
    private final Path out;
    private final Set<Path> processedFiles = new CopyOnWriteArraySet<>();
    private final Worker worker = new Worker();

    public Scanner(ScheduledExecutorService scheduler, long scanningPeriod, TimeUnit scanningTimeUnit, Path in, Path out) {
        this.scheduler = scheduler;
        this.scanningPeriod = scanningPeriod;
        this.scanningTimeUnit = scanningTimeUnit;
        this.in = in;
        this.out = out;
    }

    public void start() {
        scheduler.scheduleAtFixedRate(() -> {
            try(Stream<Path> files = Files.walk(in)) {
                files.filter(Files::isRegularFile).filter(file -> !processedFiles.contains(file)).parallel().forEach(file -> {
                    log.info("Processing file: {}...", file);
                    try(Stream<String> lines = Files.lines(file)) {
                        Stream<String> words = lines.flatMap(line -> Stream.of(line.split(" +")));
                        WorkerResult result = worker.process(words);
                        log.info("Done processing file: {}", file);
                        processedFiles.add(file);
                        try {
                            Files.createDirectory(out);
                        } catch (FileAlreadyExistsException ignored) {}
                        Path outFile = Paths.get(out.toString(), file.getFileName().toString());
                        log.info("Writing to file: {}...", outFile);
                        Files.write(outFile, result.toString().getBytes(StandardCharsets.UTF_8));
                    } catch (IOException io) {
                        log.error(io.toString());
                    }
                });
            } catch (IOException io) {
                log.error(io.toString());
            }
        }, 0, scanningPeriod, scanningTimeUnit);
    }

    public void stop() {
        scheduler.shutdown();
    }

}
