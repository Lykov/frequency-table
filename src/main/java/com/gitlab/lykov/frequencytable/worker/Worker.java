package com.gitlab.lykov.frequencytable.worker;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Отвечает за сам алгоритм подсчета слов и частоты встречаемости букв в тексте
 */
public final class Worker {
    /**
     * Запускает алгоритм подсчета слов и частоты встречаемости букв в тексте на стриме слов.
     * ПРИМЕЧАНИЕ: считать ли 'A' и 'a' разными буквами? В задании не сказано явно, поэтому посчитаем за одну :)
     * Если это не так, достаточно убрать toLowerCase
     */
    public WorkerResult process(Stream<String> words) {
        AtomicInteger wordCount = new AtomicInteger();
        Map<Character, Integer> table = words.map(word -> {
            wordCount.getAndIncrement();
            return word.toLowerCase().trim()
                    .chars()
                    .mapToObj(c -> (char) c)
                    .filter(Character::isLetter)
                    .collect(Collectors.toMap(c -> c, count -> 1, Integer::sum));
        }).flatMap(m -> m.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, Integer::sum,
                        () -> new TreeMap<>(Collections.reverseOrder())));
        return new WorkerResult(table, wordCount.get());
    }
}
