package com.gitlab.lykov.frequencytable.worker;

import java.util.Map;
import java.util.StringJoiner;

/**
 * Результат работы алгоритма
 * Хранит информацию о количестве слов и таблице встречаемости символов в тексте
 */
public final class WorkerResult {
    private final Map<Character, Integer> frequencyTable;
    private final int wordCount;

    public WorkerResult(final Map<Character, Integer> frequencyTable, final int wordCount) {
        this.frequencyTable = frequencyTable;
        this.wordCount = wordCount;
    }

    public int getWordCount() {
        return wordCount;
    }

    public Map<Character, Integer> getFrequencyTable() {
        return frequencyTable;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("Words in file: " + wordCount);
        frequencyTable.forEach((letter, count) -> joiner.add(letter + " - " + count));
        return joiner.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkerResult)) {
            return false;
        }
        WorkerResult other = (WorkerResult) obj;
        return getWordCount() == other.getWordCount() && getFrequencyTable().equals(other.getFrequencyTable());

    }
}
